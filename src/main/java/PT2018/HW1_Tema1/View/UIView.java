package PT2018.HW1_Tema1.View;

import javax.swing.*;

/**
 * The User Interface Collection of all the object in the scene
 */
public class UIView {
    private JPanel mainPanel;
    private JTextField inputText;
    private JEditorPane  historyText;
    private JLabel labelText;
    private JButton addButton;
    private JButton subButton;
    private JButton differentiateButton;
    private JButton integrateButton;
    private JButton multiplyButton;
    private JButton divideButton;

    public UIView() {
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public JTextField getInputText() {
        return inputText;
    }

    public void setInputText(JTextField inputText) {
        this.inputText = inputText;
    }

    public JEditorPane getHistoryText() {
        return historyText;
    }

    public void setHistoryText(JEditorPane historyText) {
        this.historyText = historyText;
    }

    public JLabel getLabelText() {
        return labelText;
    }

    public void setLabelText(JLabel labelText) {
        this.labelText = labelText;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public void setAddButton(JButton addButton) {
        this.addButton = addButton;
    }

    public JButton getSubButton() {
        return subButton;
    }

    public void setSubButton(JButton subButton) {
        this.subButton = subButton;
    }

    public JButton getDifferentiateButton() {
        return differentiateButton;
    }

    public void setDifferentiateButton(JButton differentiateButton) {
        this.differentiateButton = differentiateButton;
    }

    public JButton getIntegrateButton() {
        return integrateButton;
    }

    public void setIntegrateButton(JButton integrateButton) {
        this.integrateButton = integrateButton;
    }

    public JButton getMultiplyButton() {
        return multiplyButton;
    }

    public void setMultiplyButton(JButton multiplyButton) {
        this.multiplyButton = multiplyButton;
    }

    public JButton getDivideButton() {
        return divideButton;
    }

    public void setDivideButton(JButton divideButton) {
        this.divideButton = divideButton;
    }
}
