package PT2018.HW1_Tema1.View;


import PT2018.HW1_Tema1.Controller.Controller;

import javax.swing.*;
import java.awt.*;

public class PGUI extends JFrame{

    private static final int WINDOWWIDTH = 1000;
    private static final int WINDOWHEIGHT = 400;
    private UIView uiView;
    private Controller controller;

    /**
     * Creates a new User Interface following the MVC model
     * @param uiView The uiView representing the object to be drawn in the scene
     * @param controller The controller class which containg the listeners
     */
    public PGUI(UIView uiView, Controller controller){
        this.uiView = uiView;
        this.controller = controller;
        initGUI();
    }

    /**
     * Draws the User Interface and initializes the listeners
     */
    public final void initGUI() {
        uiView.setMainPanel( new JPanel());

        //TODO:move actionListeners to com.company.Controller

        uiView.getMainPanel().setLayout(new BoxLayout(uiView.getMainPanel(),BoxLayout.Y_AXIS));

        JPanel panel1 = new JPanel();//
        panel1.setLayout(new BoxLayout(panel1,BoxLayout.X_AXIS));


        JPanel panelText = new JPanel();
        panelText.setLayout(new BoxLayout(panelText,BoxLayout.Y_AXIS));
        panelText.setMinimumSize(new Dimension(8*WINDOWWIDTH/10,WINDOWHEIGHT));
        panelText.setMaximumSize(new Dimension(8*WINDOWWIDTH/10,WINDOWHEIGHT));

        JPanel panelButton = new JPanel();
        panelButton.setLayout(new BoxLayout(panelButton,BoxLayout.Y_AXIS));
        panelButton.setMinimumSize(new Dimension(2*WINDOWWIDTH/10,WINDOWHEIGHT));
        panelButton.setMaximumSize(new Dimension(2*WINDOWWIDTH/10,WINDOWHEIGHT));

        uiView.setHistoryText( new JEditorPane ());

        uiView.getHistoryText().setEditable(false);
        uiView.getHistoryText().setVisible(true);
        uiView.getHistoryText().setMinimumSize(new Dimension(8*WINDOWWIDTH/10,9*WINDOWHEIGHT/10));
        //uiView.getHistoryText().setText("First Polynomial:\nOperation:\nSecond Polynomial:\n");

        uiView.setInputText(new JTextField(200));

        uiView.getInputText().addActionListener(controller);
        uiView.getInputText().setMinimumSize(new Dimension(8*WINDOWWIDTH/10,1*WINDOWHEIGHT/10));
        uiView.getInputText().setVisible(true);

        uiView.setLabelText(new JLabel());

        uiView.getLabelText().setText("Polynomial: ");

        uiView.getLabelText().setVisible(true);
        uiView.getLabelText().setText("Polynomial: ");

        JPanel panel2  = new JPanel();

        panel2.setLayout(new BoxLayout(panel2,BoxLayout.X_AXIS));
        panel2.setMaximumSize(new Dimension(7*WINDOWWIDTH/10,3*WINDOWHEIGHT/10));
        panel2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        panel2.add(uiView.getLabelText());
        panel2.add(new JSeparator(SwingConstants.VERTICAL));
        panel2.add(uiView.getInputText());
        panel2.add(new JSeparator(SwingConstants.VERTICAL));

        panelText.add(uiView.getHistoryText());
        panelText.add(panel2);

        uiView.setAddButton(new JButton());
        uiView.getAddButton().addActionListener(controller);
        uiView.getAddButton().setText("Add");
        uiView.getAddButton().setMaximumSize(new Dimension(7*WINDOWWIDTH/10,1*WINDOWHEIGHT/10) );
        //addButton.

        uiView.setSubButton(new JButton());
        uiView.getSubButton().addActionListener(controller);
        uiView.getSubButton().setMaximumSize(new Dimension(7*WINDOWWIDTH/10,1*WINDOWHEIGHT/10) );
        uiView.getSubButton().setVisible(true);
        uiView.getSubButton().setText("Subtract");

        uiView.setDifferentiateButton(new JButton());
        uiView.getDifferentiateButton().addActionListener(controller);
        uiView.getDifferentiateButton().setMaximumSize(new Dimension(7*WINDOWWIDTH/10,1*WINDOWHEIGHT/10) );
        uiView.getDifferentiateButton().setVisible(true);
        uiView.getDifferentiateButton().setText("Differentiate");

        uiView.setIntegrateButton( new JButton());
        uiView.getIntegrateButton().addActionListener(controller);
        uiView.getIntegrateButton().setMaximumSize(new Dimension(7*WINDOWWIDTH/10,1*WINDOWHEIGHT/10) );
        uiView.getIntegrateButton().setVisible(true);
        uiView.getIntegrateButton().setText("Integrate");

        uiView.setMultiplyButton(new JButton());
        uiView.getMultiplyButton().addActionListener(controller);
        uiView.getMultiplyButton().setMaximumSize(new Dimension(7*WINDOWWIDTH/10,1*WINDOWHEIGHT/10) );
        uiView.getMultiplyButton().setVisible(true);
        uiView.getMultiplyButton().setText("Multiply");

        uiView.setDivideButton(new JButton());
        uiView.getDivideButton().addActionListener(controller);
        uiView.getDivideButton().setMaximumSize(new Dimension(7*WINDOWWIDTH/10,1*WINDOWHEIGHT/10) );
        uiView.getDivideButton().setVisible(true);
        uiView.getDivideButton().setText("Divide");

        panelButton.add(uiView.getAddButton());
        panelButton.add(new JSeparator(SwingConstants.HORIZONTAL));
        panelButton.add(uiView.getSubButton());
        panelButton.add(new JSeparator(SwingConstants.HORIZONTAL));
        panelButton.add(uiView.getMultiplyButton());
        panelButton.add(new JSeparator(SwingConstants.HORIZONTAL));
        panelButton.add(uiView.getDivideButton());
        panelButton.add(new JSeparator(SwingConstants.HORIZONTAL));
        panelButton.add(uiView.getDifferentiateButton());
        panelButton.add(new JSeparator(SwingConstants.HORIZONTAL));
        panelButton.add(uiView.getIntegrateButton());
        panelButton.add(new JSeparator(SwingConstants.HORIZONTAL));

        panel1.add(panelButton);
        panel1.add(panelText);

        uiView.getMainPanel().add(panel1);

        setContentPane(uiView.getMainPanel());
        setTitle("Polynomial Calculator");
        setSize(WINDOWWIDTH, WINDOWHEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
