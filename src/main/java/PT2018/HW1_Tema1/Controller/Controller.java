package PT2018.HW1_Tema1.Controller;


import PT2018.HW1_Tema1.Model.Polynom;
import PT2018.HW1_Tema1.View.UIView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {

    private static final String OPERATIONS = "+-/*'&=";
    private UIView uiView;
    private int lastOperation = 0;//0-none
    private Polynom lastPolynom=Polynom.ZERO;

    /**
     * Creates a new com.company.Controller class that uses the Collection of elements used in Scene
     * @param uiView
     */
    public Controller(UIView uiView) {
        this.uiView = uiView;
    }

    /**
     * Handles the event depending on the caller of that event
     * @param e The event to be handled
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()== uiView.getAddButton())
            addEvent(null);
        if(e.getSource()== uiView.getSubButton())
            subEvent(null);
        if(e.getSource()== uiView.getDifferentiateButton())
            differentiateEvent();
        if(e.getSource()== uiView.getIntegrateButton())
            integrateEvent();
        if(e.getSource()== uiView.getDivideButton())
            divideEvent(null);
        if(e.getSource()== uiView.getMultiplyButton())
            multiplyEvent(null);
        if(e.getSource()== uiView.getInputText())
            inputTextEvent();
    }

    /**
     * Sets the next operation to be handled to be is addition.If the given Polynomial is not null
     * the operation is done between the last polynomial to be handled and the given one.The last polynomial is updated
     * and  result is used in the User Interface.
     * @param p The polynomial to be added
     */
    public void addEvent(Polynom p){
        lastOperation = 1;
        uiView.getHistoryText().setText(uiView.getHistoryText().getText() + "\n+");
        if(p==null)return;
        String text = "("+lastPolynom.toString()+") + (" +p.toString()+") = ";
        lastPolynom = lastPolynom.add(p);
        text+=lastPolynom.toString();
        uiView.getHistoryText().setText(text);
    }

    /**
     * Sets the next operation to be handled to be is substraction.If the given Polynomial is not null
     * the operation is done between the last polynomial to be handled and the given one.The last polynomial is updated
     * and  result is used in the User Interface.
     * @param p The polynomial to be substracted
     */
    public void subEvent(Polynom p){
        lastOperation = 2;
        uiView.getHistoryText().setText(uiView.getHistoryText().getText() + "\n-");
        if(p==null)return;
        String text = "("+lastPolynom.toString()+") - (" +p.toString()+") = ";
        lastPolynom = lastPolynom.subtract(p);
        text+=lastPolynom.toString();
        uiView.getHistoryText().setText(text);
    }

    /**
     * Sets the next operation to be handled to be is division.If the given Polynomial is not null
     * the operation is done between the last polynomial to be handled and the given one.The last polynomial is updated
     * and  result is used in the User Interface.
     * @param p The divider polynomial
     */
    public void divideEvent(Polynom p){
        lastOperation = 3;
        uiView.getHistoryText().setText(uiView.getHistoryText().getText() + "\n/");
        if(p==null)return;
        String text = "("+lastPolynom.toString()+") / (" +p.toString()+") = ";
        Polynom[] result = lastPolynom.divide(p);
        if(result!=null) {
            if(result[0]!=null) {
                lastPolynom = result[0];
                text += lastPolynom.toString();
            }
            if (result[1]!=null)
                text += " REMAINDER: "+result[1].toString();

        }
        else
            text += "DIVISON NOT POSSIBLE!";
        uiView.getHistoryText().setText(text);
    }

    /**
     * Sets the next operation to be handled to be is multiplication.If the given Polynomial is not null
     * the operation is done between the last polynomial to be handled and the given one.The last polynomial is updated
     * and  result is used in the User Interface.
     * @param p The multiplier polynomial
     */
    public void multiplyEvent(Polynom p){
        lastOperation = 4;
        uiView.getHistoryText().setText(uiView.getHistoryText().getText() + "\n*");
        if(p==null)return;
        String text = "("+lastPolynom.toString()+") * (" +p.toString()+") = ";
        lastPolynom = lastPolynom.multiply(p);
        text+=lastPolynom.toString();
        uiView.getHistoryText().setText(text);
    }

    /**
     * Depending if the last polynomial to be handled exists then it is integrated and results is used in the
     * User interface
     */
    public void integrateEvent(){
        if(lastPolynom==null) {
            JOptionPane.showMessageDialog(null, "Please introduce a Polynom");
            return;
        }
        String text = "("+lastPolynom.toString()+") integrated by X = ";
        lastPolynom = lastPolynom.integrate();
        text+=lastPolynom.toString();
        uiView.getHistoryText().setText(text);
    }

    /**
     * Depending if the last polynomial to be handled exists then it is differentiated and results is used in the
     * User interface
     */
    public void differentiateEvent(){
        if(lastPolynom==null) {
            JOptionPane.showMessageDialog(null, "Please introduce a Polynom");
            return;
        }
        String text = "("+lastPolynom.toString()+") differentiate d by X = ";
        lastPolynom = lastPolynom.differentiate ();
        if(lastPolynom==null)
            text += "0";
        else
            text+=lastPolynom.toString();
        uiView.getHistoryText().setText(text);
    }

    /**
     * Handles the event of TextField either parsing the given polynomial or using the shortcut for the operations
     */
    public void inputTextEvent(){
        String text = uiView.getInputText().getText();
        if(!text.equals(""))
            if(OPERATIONS.contains(text.substring(0,1)) && text.length()==1)
                getOperation(text.substring(0,1));
            else
            {
                Polynom newPolynom = Polynom.parsePolynom(text);
                uiView.getInputText().setText("");
                if(newPolynom ==null){
                    JOptionPane.showMessageDialog(null,"Wrong Polynom format!");
                    return;
                }
                uiView.getHistoryText().setText(uiView.getHistoryText().getText() + "\n"+ newPolynom.toString());
                if(lastPolynom==null || lastOperation==0) {
                    lastPolynom = newPolynom;
                    uiView.getHistoryText().setText("Polynomial = "+lastPolynom.toString()+"\n");
                    return;
                }
                useOperation(newPolynom);

            }

    }

    /**
     * Returns the uiView of the Scene
     * @return Returns the uiView of the Scene
     */
    public UIView getUiView() {
        return uiView;
    }

    /**
     * Sets the UIVIew of the scene with the given data
     * @param uiView The uiView to be set
     */
    public void setUiView(UIView uiView) {
        this.uiView = uiView;
    }

    /**
     * Depending of the given operation it calls the coresponding event
     * @param op
     */
    private void getOperation(String op){
        switch (op){
            case "=":
                lastOperation = 0;
                uiView.getHistoryText().setText(uiView.getHistoryText().getText() + "\n =");
                break;
            case "+": //add
                addEvent(null);
                break;
            case "-": //subtract
                subEvent(null);
                break;
            case "/": //divide
                divideEvent(null);
                break;
            case "*": //multiply
                multiplyEvent(null);
                break;
            case "'": //differentiate 
                differentiateEvent();
                break;
            case "&": //integrate
                integrateEvent();
                break;
        }
        uiView.getInputText().setText("");
    }

    /**
     * It uses the last operation set and calls the event using the given Polynomial
     * @param p The polynomial to be used
     */
    private void useOperation(Polynom p){
        switch (lastOperation){
            case 1:
                addEvent(p);
                break;
            case 2:
                subEvent(p);
                break;
            case 3:
                divideEvent(p);
                break;
            case 4:
                multiplyEvent(p);
                break;
        }
        lastOperation = 0;
    }
}
