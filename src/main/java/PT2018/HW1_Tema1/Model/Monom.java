package PT2018.HW1_Tema1.Model;

public class Monom implements Comparable<Monom>,Cloneable{
    private float coefficient=0;
    private float power=0;
    public static final Monom ZERO = new Monom(0,0);
    /**
     * Creates a new Monom with the given coefficient and power
     * @param coefficient The coefficient of the Monom
     * @param power The power of the Monom
     */
    public Monom(float coefficient, float power) {
        this.coefficient = coefficient;
        this.power = power;
    }

    /**
     * Creates a new monom
     */
    public Monom() {
    }

    /**
     * Returns the coefficient of the current Monom
     * @return The coefficient of the current Monom
     */
    public float getCoefficient() {
        return coefficient;
    }

    /**
     * Sets the coefficient of the Monom with the given data
     * @param coefficient The new coefficient of the Monom
     */
    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }

    /**
     * Returns the power of the current Monom
     * @return The power of the current Monom
     */
    public float getPower() {
        return power;
    }

    /**
     * Sets the power of the Monom with the given data
     * @param power The power of the Monom with the given data
     */
    public void setPower(float power) {
        this.power = power;
    }

    /**
     * Returns a new Monom representing the result of the sum of the current and the given Monom
     * @param m The given Monom
     * @return A new Monom representing the result of the sum of the current and the given Monom
     */
    public Monom add(Monom m){
        if(m==null || this.power!=m.power)return null;
        return new Monom(this.coefficient+m.coefficient,this.power);
    }

    /**
     * Returns a new Monom representing the result of the substraction of the current and the given Monom
     * @param m The given Monom
     * @return A new Monom representing the result of the substraction of the current and the given Monom
     */
    public Monom subtract(Monom m){
        if(m==null || this.power!=m.power)return null;
        return new Monom(this.coefficient-m.coefficient,this.power);
    }

    /**
     * Returns a new Monom representing the result of the multiplication of the current and the given Monom
     * @param m The multiplier Monom
     * @return A new Monom representing the result of the multiplication of the current and the given Monom
     */
    public Monom multiply(Monom m){
        if(m==null)return null;
        return new Monom(this.getCoefficient()*m.getCoefficient(),this.getPower()+m.getPower());
    }

    /**
     * Returns a new Monom representing the result of the division of the current and the given Monom
     * @param m The given Monom
     * @return A new Monom representing the result of the division of the current and the given Monom
     */
    public Monom divide(Monom m){
        if(m==null && this.getPower() >= m.getPower())return null;
        return new Monom(this.getCoefficient()/m.getCoefficient(),this.getPower()-m.getPower());
    }

    /**
     * Differentiate the current Monom
     * @return The derivative of the current Monom
     */
    public Monom differentiate(){
        return new Monom(this.getCoefficient()*this.getPower(),this.getPower()-1);
    }

    /**
     * Integrate the current Monom
     * @return The integration of the current Monom
     */
    public Monom integrate(){
        return new Monom(this.getCoefficient()/(this.getPower()+1),this.getPower()+1);
    }

    /**
     * Returns a represenation of the current Monom in String format
     * @return A represenation of the current Monom in String format
     */
    @Override
    public String toString() {
        String result="";
        if(coefficient!=1 || (power==0  && coefficient==1))
         result = Float.toString(coefficient);
        if(coefficient==-1 && power!=0)
            result="-";
        if(power>0)
            result+="X"+((power==1)?"":"^"+Float.toString(power));
        return result;
    }

    /**
     * Compares the current Monom with a given Monom
     * @param o The given Monom
     * @return An integer representing the comparison between the two powers of the monoms
     */
    @Override
    public int compareTo(Monom o) {
        return -Float.compare(this.power,o.power);

    }

    /**
     * Clones the given Monom
     * @return A new Monom containing the same data as the previous Monom
     * @throws CloneNotSupportedException
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Monom(this.coefficient,this.power);
    }

    /**
     * Checks to see if a monom and an object are equal
     * @param o The object to be checked
     * @return True if they are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monom)) return false;

        Monom monom = (Monom) o;

        if (Float.compare(monom.coefficient, coefficient) != 0) return false;
        return Float.compare(monom.power, power) == 0;
    }

    /**
     * Returns an integer representing the hash of the current object
     * @return An integer representing the hash of the current object
     */
    @Override
    public int hashCode() {
        int result = (coefficient != +0.0f ? Float.floatToIntBits(coefficient) : 0);
        result = 31 * result + (power != +0.0f ? Float.floatToIntBits(power) : 0);
        return result;
    }
}
