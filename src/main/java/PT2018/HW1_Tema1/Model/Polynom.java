package PT2018.HW1_Tema1.Model;

import java.util.TreeSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynom implements Cloneable{
    private TreeSet<Monom> monoms;
    public static final Polynom ZERO = new Polynom(Monom.ZERO);
    /**
     * Generates a Polynom with no elements
     */
    public Polynom() {
        monoms =  new TreeSet<Monom>();
    }

    /**
     * Generates a Polynom contaning the given set of monoms
     * @param monoms A TreeSet of Monoms
     */
    public Polynom(TreeSet<Monom> monoms) {
        this.monoms = monoms;
    }

    /**
     * Generates a Polynom containing the given monoms
     * @param monomsParam A monom or a list of monoms
     */
    public Polynom(Monom... monomsParam){
        this.monoms =  new TreeSet<Monom>();
        for (Monom m:monomsParam) {
            monoms.add(m);
        }

    }

    /**
     * Returns a Set of the current polynomial monoms
     * @return The TreeSet of monoms
     */
    public TreeSet<Monom> getMonoms() {
        return monoms;
    }

    /**
     * Sets the monoms of polynomial to the given set
     * @param monoms A TreeSet of Monoms
     */
    public void setMonoms(TreeSet<Monom> monoms) {
        this.monoms = monoms;
    }

    /**
     * Returns a polynomial which is the result of the sum of two polynomials
     * @param p The Polynomial to be added
     * @return Result of the sum of the two polynomials
     */
    public Polynom add(Polynom p){
        Polynom result = (Polynom)this.clone();

        if(p==null)return result;

        for(Monom m1:p.getMonoms()) {
            boolean powerFound = false;
            Iterator<Monom> monomIterator = result.getMonoms().iterator();
            while(monomIterator.hasNext()){

                Monom m2 = monomIterator.next();

                if(m2.getPower()==m1.getPower()) {
                    result.monoms.remove(m2);
                    m2=m2.add(m1);
                    result.monoms.add(m2);

                    if(m2.getCoefficient()==0)
                        result.getMonoms().remove(m2);

                    powerFound = true;
                    break;
                }
                if(m1.getPower() > m2.getPower())
                    break;
            }

            if(powerFound==false)
                result.getMonoms().add(new Monom(m1.getCoefficient(),m1.getPower()));
        }
        if(result.getMonoms().size()==0)
            return new Polynom(new Monom(0,0));
        else
            result.getMonoms().removeIf((x)->Math.abs(x.getCoefficient())==0);
        return result;
    }

    /**
     * Returns a polynomial which is the result of the substraction of two polynomials
     * @param p The Polynomial to be substracted
     * @return Result of the substraction of the two polynomials
     */
    public Polynom subtract(Polynom p){
        Polynom result = (Polynom)this.clone();

        if(p==null)return result;

        for(Monom m1:p.getMonoms()) {
            boolean powerFound = false;
            Iterator<Monom> monomIterator = result.getMonoms().iterator();
            while(monomIterator.hasNext()){

                Monom m2 = monomIterator.next();

                if(m2.getPower()==m1.getPower()) {
                    result.monoms.remove(m2);
                    m2=m2.subtract(m1);
                    result.monoms.add(m2);

                    if(m2.getCoefficient()==0)
                        result.getMonoms().remove(m2);

                    powerFound = true;
                    break;
                }
                if(m1.getPower() > m2.getPower())
                    break;
            }

            if(powerFound==false)
                result.getMonoms().add(new Monom(-m1.getCoefficient(),m1.getPower()));
        }
        if(result.getMonoms().size()==0)
            return new Polynom(new Monom(0,0));
        else
            result.getMonoms().removeIf((x)->Math.abs(x.getCoefficient())==0);
        return result;
    }

    /**
     * Returns a polynomial which is the result of the multiplication of two polynomials
     * @param p The Polynomial to be multiplied by
     * @return Result of the multiplication of the two polynomials
     */
    public Polynom multiply(Polynom p){
        Polynom result = new Polynom();

        if(p==null)return result;

        for(Monom m1 : this.getMonoms())
            for(Monom m2 : p.getMonoms())
            {
                Monom data = m1.multiply(m2);

                if(data.getCoefficient()==0) // element is useless
                    continue;

                boolean exists =false;
                Iterator<Monom> iter = result.getMonoms().iterator();
                while(iter.hasNext()) {
                    Monom m3 = iter.next();
                    if (m3.getPower() == data.getPower()) {
                        Monom sum = m3.add(data);
                        m3.setCoefficient(sum.getCoefficient());
                        if (sum.getCoefficient() == 0)
                            result.getMonoms().remove(m3);
                        exists = true;
                        break;

                    }
                }

                if(!exists && data.getCoefficient()!=0)
                result.getMonoms().add(data);
            }
        if(result.getMonoms().size()==0)
            return new Polynom(new Monom(0,0));
        else
            result.getMonoms().removeIf((x)->Math.abs(x.getCoefficient())==0);
        return result;
    }

    /**
     * Returns a list which contains two polynomials representing the result of the division of the
     * current dividend polynomial and the divisor polynomial
     * @param p The divisor Polynom
     * @return An array of Polynoms which contains the quotient and remainder.
     */
    public Polynom[] divide(Polynom p){
        Polynom quotient = new Polynom();
        Polynom remainder;

        if(p==null)return null;

        if(p.equals(Polynom.ZERO) || this.equals(Polynom.ZERO)) //0/0
            return new Polynom[]{new Polynom(new Monom(0,0)),new Polynom(new Monom(0,0))};

        //the division is not possible
        if(this.getMonoms().iterator().next().getPower() < p.getMonoms().iterator().next().getPower())
            return new Polynom[]{new Polynom(new Monom(0,0)),new Polynom(this.getMonoms())};

        //clone the polinoms
        Polynom firstPolynom = (Polynom)this.clone();
        Polynom secondPolynom = (Polynom)p.clone();

       while(firstPolynom.getMonoms().iterator().next().getPower() >= secondPolynom.getMonoms().iterator().next().getPower()
               && !firstPolynom.equals(Polynom.ZERO)){

            Monom newMonom = firstPolynom.getMonoms().iterator().next().divide(secondPolynom.getMonoms().iterator().next());

            quotient.getMonoms().add(newMonom);
            firstPolynom = firstPolynom.subtract(secondPolynom.multiply(new Polynom(newMonom)));
            if(firstPolynom.equals(Polynom.ZERO))
                break;
       }


       remainder = firstPolynom;
       return new Polynom[]{quotient,remainder};
    }

    /**
     * Differentiate the current polynomial returning the result
     * @return The derivated Polynomial
     */
    public Polynom differentiate(){
        Polynom result = new Polynom();
        for(Monom m : this.getMonoms())
        {
            Monom data = m.differentiate();
            if(data.getCoefficient()!=0)
            result.getMonoms().add(data);
        }
        if(result.getMonoms().size()==0)
            return new Polynom(new Monom(0,0));
        else
            result.getMonoms().removeIf((x)->Math.abs(x.getCoefficient())==0);
        return result;
    }

    /**
     * Integrates the current polynomial returning the result
     * @return The Integrate of the current Polynomial
     */
    public Polynom integrate(){
        Polynom result = new Polynom();
        for(Monom m : this.getMonoms())
        {
            Monom data = m.integrate();
            if(data.getCoefficient()!=0)
                result.getMonoms().add(data);
        }
        if(result.getMonoms().size()==0)
            return new Polynom(new Monom(0,0));
        else
            result.getMonoms().removeIf((x)->Math.abs(x.getCoefficient())==0);
        return result;
    }

    /**
     * Parses the input string returning a polynomial based on it
     * @param input A String which respects the format of a polynom: (+-)coeff*x^power (+-) ..
     * @return A polynomial representing the parse string
     */
    public static final Polynom parsePolynom(String input){
        TreeSet<Monom> monoms = new TreeSet<>();
        input = input.replaceAll("\\s","");
        Pattern p = Pattern.compile("(?<Sign>[-+])*(?<Coef>\\d+)*[\\\\*]*(?<XGroup>[xX])*[\\\\^]*(?<Power>\\d+)*");
        Matcher matcher =  p.matcher(input);
        while(matcher.find()){
            String strSign = matcher.group("Sign");
            String strCoef = matcher.group("Coef");
            String strXgroup = matcher.group("XGroup");
            //why do I use X you ask?.Well to tell if it's -7 or -7x
            String strPower = matcher.group("Power");

            int coef = 1;
            if(strCoef!=null)
                coef = Integer.parseInt(strCoef);
            if(strSign!=null)
                coef = (strSign.equals("-")?(-coef):coef);
            int power = 0;
            if(strXgroup!=null)
                power = 1;
            if(strPower!=null)
                power = Integer.parseInt(strPower);
            Monom newMonom = new Monom();
            newMonom.setCoefficient(coef);
            newMonom.setPower(power);

            if(strSign==null && strCoef==null && strXgroup==null && strPower==null)
                continue;
            monoms.add(newMonom);
        }
        return new Polynom(monoms);
    }

    /**
     * Returns a string representing the current polynomial
     * @return String representing the current polynomial
     */
    @Override
    public String toString() {
        String result = "";
        if(this.getMonoms().size()<1)
            return result;

        Iterator<Monom> iter = this.getMonoms().iterator();
        Monom m = iter.next();
        result+=m.toString();
        while(iter.hasNext()) {
            m = iter.next();
            result += (m.getCoefficient()<0)?m.toString():"+"+m.toString();
        }
        return result;
    }

    /**
     * Clones the current object
     * @return The clone of the Object
     */
    @Override
    protected Object clone(){
        TreeSet<Monom> monoms = new TreeSet<>();
        try {

            for (Monom m : this.monoms)
                if(!m.equals(new Monom(0,0)))
                    monoms.add((Monom) m.clone());
        }
        catch (CloneNotSupportedException e){
            System.out.println(e.getMessage());
        }
        return new Polynom(monoms);
    }

    /**
     * Test to see if the current polynomial is equal to a given object
     * @param o The object to be tested
     * @return true if the two object are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Polynom)) return false;

        Polynom polynom = (Polynom) o;

        return monoms != null ? monoms.equals(polynom.monoms) : polynom.monoms == null;
    }

    /**
     * Returns an integer representing the hash of the current object
     * @return An integer representing the hash of the current object
     */
    @Override
    public int hashCode() {
        return monoms != null ? monoms.hashCode() : 0;
    }
}