package PT2018.HW1_Tema1;

import PT2018.HW1_Tema1.Controller.Controller;
import PT2018.HW1_Tema1.View.PGUI;
import PT2018.HW1_Tema1.View.UIView;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        UIView mvcUIView = new UIView();
        Controller mvcController = new Controller(mvcUIView);
        PGUI graphics = new PGUI(mvcUIView,mvcController);
        graphics.setVisible(true);
    }
}
