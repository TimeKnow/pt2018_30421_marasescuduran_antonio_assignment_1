package PT2018.HW1_Tema1;

import PT2018.HW1_Tema1.Model.Monom;
import PT2018.HW1_Tema1.Model.Polynom;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

//JUnit4.x

public class PolynomTest {
    @org.junit.Test
    public void add() throws Exception {

        Polynom v11 = Polynom.parsePolynom("3*x^2-x+1");
        Polynom v21 = Polynom.parsePolynom("6*x^3-3*x^2+2*x^1-1");
        Polynom e1  = Polynom.parsePolynom("6x^3+x");
        assertEquals(e1,v11.add(v21));

        Polynom v12 = Polynom.parsePolynom("-x^2+5x+6");
        Polynom v22 = Polynom.parsePolynom("x^2+x+7");
        Polynom e2 = Polynom.parsePolynom("6x+13");
        assertEquals(e2,v12.add(v22));

        Polynom v13 = Polynom.parsePolynom("x+3");
        Polynom v23 = Polynom.parsePolynom("-x-3");
        Polynom e3 = new Polynom(new Monom(0,0));;
        assertEquals(e3,v13.add(v23));

        Polynom v14 = Polynom.parsePolynom("x^2-1");
        Polynom v24 = Polynom.parsePolynom("-x^2");
        Polynom e4 = Polynom.parsePolynom("-1");
        assertEquals(e4,v14.add(v24));

    }
    @org.junit.Test
    public void substract() throws Exception {
        Polynom v11 = Polynom.parsePolynom("3x^2-x-1");
        Polynom v21 = Polynom.parsePolynom("6x^3-3x^2+2x-1");
        Polynom e1 = Polynom.parsePolynom("-6x^3+6x^2-3x");
        assertEquals(e1,v11.subtract(v21));

        Polynom v12 = Polynom.parsePolynom("-x^2+5x+6");
        Polynom v22 = Polynom.parsePolynom("x^2+5x+7");
        Polynom e2 = Polynom.parsePolynom("-2x^2-1");
        assertEquals(e2,v12.subtract(v22));

        Polynom v13 = Polynom.parsePolynom("x+3");
        Polynom v23 = Polynom.parsePolynom("x+3");
        Polynom e3 = new Polynom(new Monom(0,0));
        assertEquals(e3,v13.subtract(v23));

        Polynom v14 = Polynom.parsePolynom("x^2-1");
        Polynom v24 = Polynom.parsePolynom("x^2");
        Polynom e4 = Polynom.parsePolynom("-1");
        assertEquals(e4,v14.subtract(v24));
    }

    @org.junit.Test
    public void multiply() throws Exception {
        Polynom v11 = Polynom.parsePolynom("x^2+1");
        Polynom v21 = Polynom.parsePolynom("x+3");
        Polynom e1 = Polynom.parsePolynom("x^3+3x^2+x+3");
        assertEquals(e1,v11.multiply(v21));

        Polynom v12 = Polynom.parsePolynom("x+1");
        Polynom v22 = Polynom.parsePolynom("x-1");
        Polynom e2 = Polynom.parsePolynom("x^2-1");
        assertEquals(e2,v12.multiply(v22));

        Polynom v13 = Polynom.parsePolynom("-2x^2+3");
        Polynom v23 = Polynom.parsePolynom("-6x");
        Polynom e3 = Polynom.parsePolynom("12x^3-18x");
        assertEquals(e3,v13.multiply(v23));

        Polynom v14 = Polynom.parsePolynom("x^2+2");
        Polynom v24 = Polynom.parsePolynom("-7");
        Polynom e4 = Polynom.parsePolynom("-7x^2-14");
        assertEquals(e4,v14.multiply(v24));

        Polynom v15 = Polynom.parsePolynom("x+1");
        Polynom v25 = Polynom.parsePolynom("0");
        Polynom e5 = new Polynom(new Monom(0,0));
        assertEquals(e5,v15.multiply(v25));

        Polynom v16 = Polynom.parsePolynom("7");
        Polynom v26 = Polynom.parsePolynom("7");
        Polynom e6 = Polynom.parsePolynom("49");
        assertEquals(e6,v16.multiply(v26));
    }

    @org.junit.Test
    public void divide() throws Exception {
        Polynom v11 = Polynom.parsePolynom("x^2-1");
        Polynom v21 = Polynom.parsePolynom("x+1");
        boolean notNullE1 = true;
        Polynom e11 = Polynom.parsePolynom("x-1");
        Polynom e21 = new Polynom(new Monom(0,0));;
        Polynom[] res1 = v11.divide(v21);
        assertNotNull(res1);
        if(res1!=null){
            assertEquals(e11,res1[0]);
            assertEquals(e21,res1[1]);
        }

        Polynom v12 = Polynom.parsePolynom("x^2+1");
        Polynom v22 = Polynom.parsePolynom("x^2");
        boolean notNullE2 = true;
        Polynom e12 = Polynom.parsePolynom("1");
        Polynom e22 = Polynom.parsePolynom("1");
        Polynom[] res2 = v12.divide(v22);
        assertNotNull(res1);
        if(res1!=null){
            assertEquals(e12,res2[0]);
            assertEquals(e22,res2[1]);
        }

        Polynom v13 = Polynom.parsePolynom("x^2-1");
        Polynom v23 = Polynom.parsePolynom("x^3");
        boolean notNullE3 = true;
        Polynom e13 = new Polynom(new Monom(0,0));
        Polynom e23 = new Polynom(v13.getMonoms());
        Polynom[] res3 = v13.divide(v23);
        assertNotNull(res3);
        if(res3!=null){
            assertEquals(e13,res3[0]);
            assertEquals(e23,res3[1]);
        }
    }

    @org.junit.Test
    public void differentiate() throws Exception {
        Polynom v1 = Polynom.parsePolynom("x");
        Polynom e1 = Polynom.parsePolynom("1");
        assertEquals(e1,v1.differentiate());

        Polynom v2 = Polynom.parsePolynom("1");
        Polynom e2 = new Polynom(new Monom(0,0));;
        assertEquals(e2,v2.differentiate());

        Polynom v3 = Polynom.parsePolynom("4x^3+2x^2");
        Polynom e3 = Polynom.parsePolynom("12x^2+4x");
        assertEquals(e3,v3.differentiate());
    }

    @org.junit.Test
    public void integrate() throws Exception {
        Polynom v1 = Polynom.parsePolynom("x");
        Polynom e1 = new Polynom(new Monom(1f/2,2));
        assertEquals(e1,v1.integrate());

        Polynom v2 = Polynom.parsePolynom("4x^2+1");
        Polynom e2 = new Polynom(new Monom(4f/3,3),new Monom(1,1));
        assertEquals(e2,v2.integrate());
    }


}